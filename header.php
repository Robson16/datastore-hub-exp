<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <nav id="navbar" class="navbar navbar-expand-lg bg-color-one">
        <div class="container">
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>" title="<?php echo bloginfo('title'); ?>" aria-label="<?php echo bloginfo('title'); ?>">
                <?php if (has_custom_logo()) : ?>
                    <img class="img-fluid" src="<?php echo wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0]; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
                <?php else : ?>
                    <?php bloginfo('title'); ?>
                <?php endif; ?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav" aria-controls="navbar-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'main_menu',
                    'depth' => 2,
                    'container' => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'navbar-nav',
                    'menu_class' => 'navbar-nav ml-auto',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                )
            );
            ?>
        </div>
        <!--/.container-->
    </nav>
    <!--/.navbar-->

    <header id="header" class="position-relative">
        <div class="<?php echo (wp_is_mobile()) ? 'bg-color-one' : 'bg-color-two' ?>">
            <?php get_template_part( 'template-parts/header/header', 'links' ); ?>
        </div>

        <?php get_template_part( 'template-parts/header/header', 'buynow' ); ?>
        
        <img src="<?php echo get_header_image(); ?>" alt="" class="img-fluid">
    </header>