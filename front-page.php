<?php

get_header('pink');
?>

<main id="home">

    <section id="cover">
        <?php
        $slides = get_theme_mod('setting_carousel');
        $is_mobile = wp_is_mobile();
        ?>

        <?php if ($slides) : ?>
            <div id="cover-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
                        <li data-target="#cover-carousel" data-slide-to="<?php echo $x ?>" class="<?php if ($x === 0) echo 'active'; ?>"></li>
                    <?php endfor; ?>
                </ol>
                <div class="carousel-inner">
                    <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
                        <div class="carousel-item <?php if ($x === 0) echo 'active'; ?>">
                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_src(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], 'full')[0]; ?>" alt="<?php echo get_post_meta(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], '_wp_attachment_image_alt', TRUE); ?>">
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
            <!-- /#cover-corousel -->
        <?php endif; ?>

    </section>
    <!-- /#cover -->

    <section id="about" class="position-relative bg-image-white">
        <div class="container py-5">
            <?php
            while ( have_posts() ) {
                the_post();

                get_template_part( 'template-parts/content/content', 'page' );
            }
            ?>
        </div>
        <!-- /.container -->
    </section>

    <?php
    $speakers = new WP_Query( array(
        'post_type' => array( 'speakers', 'panelists' ),
        'post_status' => 'publish',
        'posts_per_page' => 10,
    ) );

    if ( $speakers->have_posts() ) :
    ?>
        <section id="speakers" class="position-relative bg-image-lightgreen">
            <div class="container py-5">
                <div class="row mb-5">
                    <div class="col">
                        <h2 class='h1 text-lowercase text-color-three font-cocogoose'>Palestrantes e Painelistas</h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <?php while ( $speakers->have_posts() ) : $speakers->the_post() ?>
                        <div class="col-12 col-md-6">
                            <?php echo get_template_part( 'template-parts/content/content', 'speaker' ); ?>
                        </div>
                        <!-- /.col -->
                    <?php endwhile; ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>

    <?php endif; ?>

</main>

<?php
get_footer('bigger');
