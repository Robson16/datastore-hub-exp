<div class="bg-dark text-white">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-md-6">
                <p class="text-center text-md-left m-0" style="font-size: 1rem;">
                    &copy; <a href="https://www.datastore.com.br/" target="_blank" rel="noopener">DataStore, <?php echo date_i18n('Y'); ?> - Todos os direitos reservados</a>
                </p>
            </div>
            <!-- /.col -->
            <div class="position-relative col-12 col-md-6">
                <p class="text-center text-md-right m-0" style="font-size: 1rem;">
                    Design por <a href="https://www.stratesign.com.br/" target="_blank" rel="noopener">Stratesign</a>
                </p>
                <p style="position: absolute; margin: 0; font-size: 1rem; color: transparent;">
                    Desenvolvido por <a href="https://www.robsonhrodrigues.com.br/" style="text-decoration: none;" target="_blank" rel="noopener">Robson H. Rodrigues</a>
                </p>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>