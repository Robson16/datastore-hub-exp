<div class="bg-dark">
    <div class="container py-3">
        <div class="row">
            <div class="col-12 col-md-2 d-flex flex-column flex-md-row align-items-center justify-content-center mb-3 mb-md-0">
                <img class="img-fluid" 
                src="<?php echo wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0]; ?>" 
                alt="<?php bloginfo( 'name' ); ?>" 
                title="<?php bloginfo( 'name' ); ?>">
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-10">
                <ul class="d-flex justify-content-center justify-content-md-end align-items-center mb-1">
                    <li>
                        <a href="<?php echo get_theme_mod( 'setting_facebook' ); ?>" target="_blank" rel="noopener" aria-label="Facebook DataStore HUB EXP">
                            <i class="text-white fab fa-facebook-f fa-lg px-2"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_theme_mod( 'setting_instagram' ); ?>" target="_blank" rel="noopener" aria-label="Instagram DataStore HUB EXP">
                            <i class="text-white fab fa-instagram fa-lg px-2"></i>
                        </a>
                    </li>
                </ul>
                <?php 
                wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu_one',
                        'depth' => 1,
                        'container' => 'div',
                        'container_class' => 'footer-nav',
                        'container_id' => 'footer-nav-one',
                        'menu_class' => 'd-flex flex-column flex-md-row align-items-center justify-content-end text-color-four m-0 p-0'
                    )
                );
                wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu_two',
                        'depth' => 1,
                        'container' => 'div',
                        'container_class' => 'footer-nav',
                        'container_id' => 'footer-nav-two',
                        'menu_class' => 'd-flex flex-column flex-md-row align-items-center justify-content-end m-0 p-0'
                    )
                );
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>