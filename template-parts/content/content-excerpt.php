<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'my-5' ); ?>>
	<header class="entry-header">
		<?php
		if ( is_sticky() && is_home() && ! is_paged() ) {
			printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'dtshubxp20' ) );
		}
		the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		?>
	</header><!-- .entry-header -->

	<figure>
		<a href="<?php echo esc_url( get_permalink() ); ?>">
			<?php 
			the_post_thumbnail('full', array(
				'class' => 'img-fluid',
				'title' => get_the_title()
			) );
			?>
		</a>
	</figure>	

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<a class="btn btn-color-one btn-lg" href="<?php echo esc_url( get_permalink() ); ?>"><?php _e( 'Veja Mais', 'dtshubxp20' ) ?></a>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
