<article id="speaker-<?php the_ID(); ?>" class="speaker">
    <div class="row">
        <div class="<?php echo (is_single()) ? 'col-12 col-md-3' : 'col-6'; ?>">

            <?php if ( has_post_thumbnail() ) : ?>

                <figure title="<?php the_title_attribute(); ?>">
                    <?php if ( !is_single() ) : ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>">
                    <?php endif; ?>

                        <?php
                        the_post_thumbnail( 'medium', array(
                            'class' => 'img-fluid',
                            'alt' => get_the_title(),
                        ) );
                        ?>

                    <?php if ( !is_single() ) : ?>
                        </a>
                    <?php endif; ?>
                </figure>

            <?php else : ?>

                <figure title="<?php the_title_attribute(); ?>">
                    <a class="thumbnail" href="<?php the_permalink(); ?>">
                        <img class="img-fluid" src="https://via.placeholder.com/384" alt="">
                    </a>
                </figure>

            <?php endif; ?>

            <div class="cross"></div>

        </div>
        <!-- /.col -->

        <div class="<?php echo ( is_single() ) ? 'col-12 col-md-9' : 'col-6'; ?>">
            <?php if ( !is_single() ) : ?>
                <a href="<?php the_permalink(); ?>">
            <?php endif; ?>

                <header class="entry-header">
                    <h3 class="title"><?php echo get_the_title() ?></h3>
                    <h4 class="sub-title"><?php the_field( 'subtitle' ); ?></h4>

                    <?php if ( is_single() && get_field( 'linkedin' ) ) : ?>
                        <a href="<?php the_field( 'linkedin' ); ?>" target="_blank" rel="noopener" aria-label="<?php echo 'Linkedin de ' . get_the_title(); ?>">
                            <i class="linkedin fab fa-linkedin fa-2x"></i>
                        </a>
                    <?php endif; ?>
                </header>

            <?php if ( !is_single() ) : ?>
                </a>
            <?php endif; ?>

            <?php if ( is_single() ) : ?>

                <div class="entry-content mt-md-5">
                    <?php the_content(); ?>
                </div>

            <?php endif; ?>
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->

</article><!-- #speaker-<?php the_ID(); ?> -->