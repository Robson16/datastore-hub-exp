<div class="header-cta">
    <div class="container">
        <div class="row">
            <div class="col col-lg-3 offset-lg-9">
                <a href="" class="cta open-form">Reserve agora sua participação!</a>
                <div class="form-wrapp">
                    <?php echo do_shortcode( get_theme_mod( 'setting_form_shortcode' ) ); ?>
                </div>
            </div>
        </div>
    </div>
</div>