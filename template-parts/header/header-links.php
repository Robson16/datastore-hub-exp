<div class="container px-3 px-md-0 px-lg-3">
    <div class="row">
        <div class="col">
            <ul class="d-flex justify-content-start align-items-center mb-0">
                <li>
                    <a href="<?php echo get_theme_mod( 'setting_facebook' ); ?>" target="_blank" rel="noopener" aria-label="Facebook DataStore HUB EXP">
                        <i class="fab fa-facebook-f fa-lg p-1"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_theme_mod( 'setting_instagram' ); ?>" target="_blank" rel="noopener" aria-label="Instagram DataStore HUB EXP">
                        <i class="fab fa-instagram fa-lg p-1"></i>
                    </a>
                </li>
                <li>
                    <p class="font-cocogoose p-1 m-0">#datastorehub2020</p>
                </li>
            </ul>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->