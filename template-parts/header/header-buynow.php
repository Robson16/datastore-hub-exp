<?php if ( get_theme_mod( 'setting_buylink_url' ) && get_theme_mod( 'setting_buylink_text' ) ):  ?>
<div class="header-cta">
    <div class="container">
        <div class="row">
            <div class="col col-lg-3 offset-lg-9">
                <a href="<?php echo esc_html( get_theme_mod( 'setting_buylink_url', 'https://www.sympla.com.br/' ) ); ?>" class="cta font-weight-bold" target="_black" rel="noopener">
                    <?php echo get_theme_mod( 'setting_buylink_text', 'Clique aqui para comprar agora!' ); ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>