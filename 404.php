<?php

/**
 * The template for displaying the 404 template.
 *
 */

get_header();
?>

<main class="container py-5">
	<div class="row">
		<div class="col-12">

			<h1><?php _e('Página não encontrada', 'dtshubxp20'); ?></h1>

			<p><?php _e('A página que você estava procurando não foi encontrada. Pode ter sido removido, renomeado ou não existir mais.', 'dtshubxp20'); ?></p>

		</div>
	</div>
</main>

<?php
get_footer();
