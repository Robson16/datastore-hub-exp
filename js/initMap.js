$(function () {
    initMap();
});

function initMap() {
    var map = new google.maps.Map(document.getElementById('contact-map'), {
        zoom: 15,
        center: {lat: -23.1887866, lng: -46.8845122}
    });

    var geocoder = new google.maps.Geocoder();
    var address = document.getElementById('contact-address').textContent;

    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}
