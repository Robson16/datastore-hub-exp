$(function () {
    // Evento para exibir ou ocultar o formulário da capa em dispositivos móveis
    $(".open-form").on("click", function (event) {
        event.preventDefault();
        $(this).next(".form-wrapp").slideToggle();
    });
});