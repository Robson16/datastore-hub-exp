<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();
?>

<main class="container">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content/content', 'single' );

			if (comments_open() || get_comments_number()) comments_template();
		}
	}

	?>

</main>

<?php get_footer('bigger'); ?>
