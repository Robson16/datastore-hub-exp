<?php

/**
 * The template for displaying archive of speakers
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();
?>

<main class="container py-5">
    <div class="row">
        <div class="col">
            <h1 class="text-lowercase font-cocogoose text-color-three mb-5"><?php echo post_type_archive_title(); ?></h1>
        </div>
    </div>
    <div class="row">
        <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : the_post(); ?>
                <div class="col-12 col-md-6">
                    <?php echo get_template_part( 'template-parts/content/content', 'speaker' ); ?>
                </div>
                <!-- /.col -->
            <?php endwhile; ?>

            <div class="col-12">
                <?php echo bootstrap_pagination(); ?>
            </div>
            <!-- /.col -->

        <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

        <?php endif; ?>

    </div>
    <!-- /.row -->
</main>
<!-- /.container -->

<?php
get_footer('bigger');
