<footer id="footer">
    <?php get_template_part( 'template-parts/footer/footer', 'navbar' ); ?>
    <?php get_template_part( 'template-parts/footer/footer', 'copyright' ); ?>
</footer>

<?php wp_footer(); ?>

</body>

</html>