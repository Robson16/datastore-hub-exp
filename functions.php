<?php

/**
 * Datastore Hub Experience 2020 functions and settings
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */
if (!function_exists('dtshubxp20_setup')) {

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function dtshubxp20_setup() {
        // Enabling translation support
        $textdomain = 'dtshubxp20';
        load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
        load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

        // Menu registration
        register_nav_menus(array(
            'main_menu' => __('Menu Principal', 'dtshubxp20'),
            'footer_menu_one' => __('Menu Um Rodapé', 'dtshubxp20'),
            'footer_menu_two' => __('Menu Dois Rodapé', 'dtshubxp20'),
        ));

        // Add theme support

        /**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
        add_theme_support('title-tag');

        /**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
        add_theme_support('custom-logo', array(
            'height'      => 50,
            'width'       => 250,
            'header-text' => array( 'site-title', 'site-description' ),
        ));

        /**
		 * Add support for core custom header image.
		 *
		 * @link https://codex.wordpress.org/Custom_Headers
		 */
        add_theme_support( 'custom-header', array(
            'width'         => 2560,
            'height'        => 530,
            'default-image' => 'https://via.placeholder.com/2560x530',
        ) );

        /**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */		
        add_theme_support( 'post-thumbnails' );
        
        /**
         * Add custom images sizes
         * 
         * @link https://developer.wordpress.org/reference/functions/add_image_size/
         */
        add_image_size( 'latest_post', 385, 385, array( 'center', 'center' ) );
        
        // Editor Color Palette
        add_theme_support('editor-color-palette', array(
            array(
                'name'  => __('Branco', 'dtshubxp20'),
                'slug'  => 'white',
                'color'    => '#ffffff',
            ),
            array(
                'name'  => __('Preto', 'dtshubxp20'),
                'slug'  => 'black',
                'color'    => '#000000',
            ),
            array(
                'name'  => __('Serpente do Mar', 'dtshubxp20'),
                'slug'  => 'sea-serpent',
                'color'    => '#56c5d7',
            ),
            array(
                'name'  => __('Pera', 'dtshubxp20'),
                'slug'  => 'pear',
                'color' => '#cadb29',
            ),
            array(
                'name'  => __('Chama', 'dtshubxp20'),
                'slug'  => 'flame',
                'color' => '#ee5325',
            ),
            array(
                'name'    => __('Rosa Framboesa', 'dtshubxp20'),
                'slug'    => 'raspberry-pink',
                'color'    => '#e44a9a',
            ),
            array(
                'name'    => __('Imperial', 'dtshubxp20'),
                'slug'    => 'imperial',
                'color'    => '#5f276e',
            ),
        ));
    }
}

add_action('after_setup_theme', 'dtshubxp20_setup');

/**
 * Enqueue scripts and styles.
 */
function dtshubxp20_scripts() {
    // CSS
    wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', NULL, '4.3.1', 'all');
    wp_enqueue_style('dtshubxp20-style', get_stylesheet_uri(), array('bootstrap'), '1.0', 'all');
    // Js
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, '3.4.1');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true);
    wp_enqueue_script('fontawesome', '//kit.fontawesome.com/5d0d8e3eb5.js', array(), '5.12.0', false);
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'dtshubxp20_scripts');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dtshubxp20_sidebars() {

    // Arguments used in all register_sidebar() calls.
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );

    // Rodapé #1.
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Rodapé #1', 'dtshubxp20'),
        'id' => 'dtshubxp20-sidebar-footer-1',
        'description' => __('Os widgets nesta área serão exibidos na primeira coluna no rodapé.', 'dtshubxp20'),
    )));

    // Rodapé #2.
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Rodapé #2', 'dtshubxp20'),
        'id' => 'dtshubxp20-sidebar-footer-2',
        'description' => __('Os widgets nesta área serão exibidos na segunda coluna no rodapé.', 'dtshubxp20'),
    )));

    // Rodapé #3.
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Rodapé #3', 'dtshubxp20'),
        'id' => 'dtshubxp20-sidebar-footer-3',
        'description' => __('Os widgets nesta área serão exibidos na terceira coluna no rodapé.', 'dtshubxp20'),
    )));

    //Rodapé #4.
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Footer #4', 'dtshubxp20'),
        'id' => 'dtshubxp20-sidebar-footer-4',
        'description' => __('Os widgets nesta área serão exibidos na quarta coluna no rodapé.', 'dtshubxp20'),
    )));
}

add_action('widgets_init', 'dtshubxp20_sidebars');

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/class/class-wp-bootstrap-navwalker.php';

/**
 *  WordPress Bootstrap Pagination
 */
require_once get_template_directory() . '/inc/wp-bootstrap4.1-pagination.php';

/**
 *  TGM Plugin Activation
 */
require_once get_template_directory() . '/inc/required-plugins.php';

/**
 * Extra functions, filters, and actions for the theme
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 *  Custom Post Types
 */
require_once get_template_directory() . '/inc/post-type.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/kirki/kirki-config.php';
