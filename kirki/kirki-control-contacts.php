<?php

Kirki::add_section('section_contacts', array(
    'title' => esc_html__('Contato', 'dtshubxp20'),
    'priority' => 160,
));

Kirki::add_field('dtshubxp20_kirki_config', [
    'type' => 'text',
    'settings' => 'setting_form_shortcode',
    'label' => __('Form Shortcode', 'dtshubxp20'),
    'description' => __('Shortcode do formulario de contato', 'dtshubxp20'),
    'section' => 'section_contacts',
    'default' => '',
    'priority' => 10,
]);
