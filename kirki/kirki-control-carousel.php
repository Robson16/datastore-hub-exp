<?php

Kirki::add_section('section_carousel', array(
    'title' => esc_html__('Carrossel da página inicial', 'dtshubxp20'),
    'priority' => 160,
));

Kirki::add_field('dtshubxp20_kirki_config', [
    'type' => 'repeater',
    'label' => esc_html__('Slides', 'dtshubxp20'),
    'section' => 'section_carousel',
    'priority' => 10,
    'row_label' => [
        'type' => 'field',
        'value' => esc_html__('Slide', 'dtshubxp20'),
        'field' => 'link_text',
    ],
    'button_label' => esc_html__('Add novo', 'dtshubxp20'),
    'settings' => 'setting_carousel',
    'fields' => [
        'slide_desktop' => [
            'type' => 'image',
            'label' => esc_html__('Slide', 'dtshubxp20'),
            'description' => esc_html__('Versão para dispositivos grandes', 'dtshubxp20'),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
        'slide_mobile' => [
            'type' => 'image',
            'label' => esc_html__('Slide Mobile', 'dtshubxp20'),
            'description' => esc_html__('Versão para dispositivos móveis', 'dtshubxp20'),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
    ]
]);
