<?php

function dtshubxp20_kirki() {
    if (class_exists('Kirki')) {
        Kirki::add_config('dtshubxp20_kirki_config', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));
        require_once get_template_directory() . '/kirki/kirki-control-carousel.php';
        require_once get_template_directory() . '/kirki/kirki-control-networks.php';
        require_once get_template_directory() . '/kirki/kirki-control-contacts.php';
        require_once get_template_directory() . '/kirki/kirki-control-miscellany.php';
    }
}

add_action('customize_register', 'dtshubxp20_kirki');
