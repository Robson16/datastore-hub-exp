<?php

Kirki::add_panel( 'panel_miscellany', array(
    'priority'  => 180,
    'title'     => esc_html__( 'Miscelânea', 'dtshubxp20' ),
    'description' => esc_html__( 'My panel description', 'dtshubxp20' ),
) );

Kirki::add_section( 'section_buylink', array(
    'title'         => esc_html__( 'Venda de Ingresso', 'dtshubxp20' ),
    'description'   => esc_html__( 'Link exibido em áreas fixas, como o cabeçalho, por exemplo.', 'dtshubxp20' ),
    'panel'         => 'panel_miscellany',
    'priority'      => 160,
) );

Kirki::add_field( 'dtshubxp20_kirki_config', array(
	'type'     => 'editor',
	'settings' => 'setting_buylink_text',
    'label'    => esc_html__( 'Texto', 'dtshubxp20' ),
    'description' => __( 'Texto para o link', 'dtshubxp20' ),
	'section'  => 'section_buylink',
	'default'  => esc_html__( 'Clique aqui para comprar agora!', 'dtshubxp20' ),
    'priority' => 10,
 ) );

Kirki::add_field( 'dtshubxp20_kirki_config', array(
	'type'     => 'link',
	'settings' => 'setting_buylink_url',
    'label'    => __( 'URL', 'dtshubxp20' ),
    'description' => __( 'Link/URL para compra', 'dtshubxp20' ),
    'section'  => 'section_buylink',
    'default'  => 'https://www.sympla.com.br/',
	'priority' => 10,
) );