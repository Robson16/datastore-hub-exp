<?php

Kirki::add_section('section_social_networks', array(
    'title' => esc_html__('Redes sociais', 'dtshubxp20'),
    'priority' => 160,
));

Kirki::add_field('dtshubxp20_kirki_config', [
    'type' => 'link',
    'settings' => 'setting_facebook',
    'label' => __('Facebook', 'dtshubxp20'),
    'description' => __('URL da página do Facebook', 'dtshubxp20'),
    'section' => 'section_social_networks',
    'default' => 'https://www.facebook.com/',
    'priority' => 10,
]);

Kirki::add_field('dtshubxp20_kirki_config', [
    'type' => 'link',
    'settings' => 'setting_instagram',
    'label' => __('Instagram', 'dtshubxp20'),
    'description' => __('URL da página do Instagram', 'dtshubxp20'),
    'section' => 'section_social_networks',
    'default' => 'https://www.instagram.com/',
    'priority' => 10,
]);
