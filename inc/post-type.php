<?php

// Register Custom Post Type
function dtshubxp20_speakers_post_type() {

	$labels = array(
		'name'                  => _x( 'Palestrantes', 'Post Type General Name', 'dtshubxp20' ),
		'singular_name'         => _x( 'Palestrante', 'Post Type Singular Name', 'dtshubxp20' ),
		'menu_name'             => __( 'Palestrantes', 'dtshubxp20' ),
		'name_admin_bar'        => __( 'Palestrante', 'dtshubxp20' ),
		'archives'              => __( 'Arquivos Palestrante', 'dtshubxp20' ),
		'attributes'            => __( 'Atributos Palestrante', 'dtshubxp20' ),
		'parent_item_colon'     => __( 'Palestrante Pai', 'dtshubxp20' ),
		'all_items'             => __( 'Todos Palestrantes', 'dtshubxp20' ),
		'add_new_item'          => __( 'Add Novo Palestrante', 'dtshubxp20' ),
		'add_new'               => __( 'Add Novo', 'dtshubxp20' ),
		'new_item'              => __( 'Novo Palestrante', 'dtshubxp20' ),
		'edit_item'             => __( 'Editar Palestrante', 'dtshubxp20' ),
		'update_item'           => __( 'Atualizar Palestrante', 'dtshubxp20' ),
		'view_item'             => __( 'Ver Palestrante', 'dtshubxp20' ),
		'view_items'            => __( 'Ver Palestrantes', 'dtshubxp20' ),
		'search_items'          => __( 'Buscar Palestrante', 'dtshubxp20' ),
		'not_found'             => __( 'Não Encontrado', 'dtshubxp20' ),
		'not_found_in_trash'    => __( 'Não Encontrado no Lixo', 'dtshubxp20' ),
		'featured_image'        => __( 'Imagem destacada', 'dtshubxp20' ),
		'set_featured_image'    => __( 'Definir imagem em destaque', 'dtshubxp20' ),
		'remove_featured_image' => __( 'Remover imagem em destaque', 'dtshubxp20' ),
		'use_featured_image'    => __( 'Use como imagem em destaque', 'dtshubxp20' ),
		'insert_into_item'      => __( 'Inserir no palestrante', 'dtshubxp20' ),
		'uploaded_to_this_item' => __( 'Carregado para este palestrante', 'dtshubxp20' ),
		'items_list'            => __( 'Lista de Palestrante', 'dtshubxp20' ),
		'items_list_navigation' => __( 'Navegação na lista de palestrantes', 'dtshubxp20' ),
		'filter_items_list'     => __( 'Filtrar lista de palestrantes', 'dtshubxp20' ),
	);
	$rewrite = array(
		'slug'                  => 'palestrante',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Palestrante', 'dtshubxp20' ),
		'description'           => __( 'Palestrantes do evento', 'dtshubxp20' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'palestrantes',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'speakers', $args );

}

add_action( 'init', 'dtshubxp20_speakers_post_type', 0 );

// Register Custom Post Type
function dtshubxp20_panelists_post_type() {

	$labels = array(
		'name'                  => _x( 'Painelistas', 'Post Type General Name', 'dtshubxp20' ),
		'singular_name'         => _x( 'Painelista', 'Post Type Singular Name', 'dtshubxp20' ),
		'menu_name'             => __( 'Painelistas', 'dtshubxp20' ),
		'name_admin_bar'        => __( 'Painelista', 'dtshubxp20' ),
		'archives'              => __( 'Arquivos Painelista', 'dtshubxp20' ),
		'attributes'            => __( 'Atributos Painelista', 'dtshubxp20' ),
		'parent_item_colon'     => __( 'Painelista Pai', 'dtshubxp20' ),
		'all_items'             => __( 'Todos Painelistas', 'dtshubxp20' ),
		'add_new_item'          => __( 'Add Novo Painelista', 'dtshubxp20' ),
		'add_new'               => __( 'Add Novo', 'dtshubxp20' ),
		'new_item'              => __( 'Novo Painelista', 'dtshubxp20' ),
		'edit_item'             => __( 'Editar Painelista', 'dtshubxp20' ),
		'update_item'           => __( 'Atualizar Painelista', 'dtshubxp20' ),
		'view_item'             => __( 'Ver Painelista', 'dtshubxp20' ),
		'view_items'            => __( 'Ver Painelistas', 'dtshubxp20' ),
		'search_items'          => __( 'Buscar Painelista', 'dtshubxp20' ),
		'not_found'             => __( 'Não Encontrado', 'dtshubxp20' ),
		'not_found_in_trash'    => __( 'Não Encontrado no Lixo', 'dtshubxp20' ),
		'featured_image'        => __( 'Imagem destacada', 'dtshubxp20' ),
		'set_featured_image'    => __( 'Definir imagem em destaque', 'dtshubxp20' ),
		'remove_featured_image' => __( 'Remover imagem em destaque', 'dtshubxp20' ),
		'use_featured_image'    => __( 'Use como imagem em destaque', 'dtshubxp20' ),
		'insert_into_item'      => __( 'Inserir no Painelista', 'dtshubxp20' ),
		'uploaded_to_this_item' => __( 'Carregado para este Painelista', 'dtshubxp20' ),
		'items_list'            => __( 'Lista de Painelista', 'dtshubxp20' ),
		'items_list_navigation' => __( 'Navegação na lista de Painelistas', 'dtshubxp20' ),
		'filter_items_list'     => __( 'Filtrar lista de Painelistas', 'dtshubxp20' ),
	);
	$rewrite = array(
		'slug'                  => 'painelista',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Painelista', 'dtshubxp20' ),
		'description'           => __( 'Painelistas do evento', 'dtshubxp20' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-microphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'painelistas',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'panelists', $args );

}

add_action( 'init', 'dtshubxp20_panelists_post_type', 0 );
