<?php
/**
 * Template Name: Fundo Azul
 * Template Post Type: post, speakers, page
 *
 */

get_header();
?>

<main class="bg-color-one">
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <?php

                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();

                    get_template_part( 'template-parts/content/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }

                endwhile; // End of the loop.
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</main>

<?php get_footer('bigger'); ?>
