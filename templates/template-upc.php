<?php

/**
 * Template Name: UPC
 * Template Post Type: page
 *
 */

get_header('flame');
?>

<main id="page-speakers" class="bg-image-upc">
    <div class="container py-5">
        <div class="row py-5">
            <div class="col-12 col-md-7 col-lg-8 d-flex flex-column justify-content-center">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/hub-e-upc.png'; ?>" alt="HUB é UPC">
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-5 col-lg-4 text-right d-flex flex-column justify-content-center">
                <h1 class="text-white font-weight-bold">Ultimate<br>Presentation<br>Chance!</h1>
                <h2 class="text-color-one font-weight-light">Com <span class="text-white">startups</span> que podem <br> mudar o mercado <br> imobiliário brasileiro!</h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <?php
        $speakers = new WP_Query( array(
            'post_type' => array( 'speakers', 'panelists' ),
            'post_status' => 'publish',
        ) );

        if ( $speakers->have_posts() ) :
        ?>
            <div class="row">
                <?php while ( $speakers->have_posts() ) : $speakers->the_post() ?>
                    <div class="col-12 col-md-6 col-lg-3 mb-3">
                        <a class="d-block h-100 border p-3 <?php if( get_field( 'confirmed' )[0] == true ) echo 'speaker-confirmed'; ?>" href="<?php the_permalink(); ?>">

                            <?php if (has_post_thumbnail()) : ?>
                                <figure title="<?php the_title_attribute(); ?>">
                                    <?php
                                    the_post_thumbnail( 'thumbnail', array(
                                        'class' => 'img-fluid',
                                        'alt' => get_the_title(),
                                    ) );
                                    ?>
                                </figure>
                            <?php else : ?>
                                <figure title="<?php the_title_attribute(); ?>">
                                    <img class="img-fluid" src="https://via.placeholder.com/150" alt="Foto do Palestrante">
                                </figure>
                            <?php endif; ?>

                            <h3 class="text-color-three font-weight-bold"><?php echo get_the_title() ?></h3>
                            <h4 class="text-white font-weight-light"><?php the_field( 'subtitle' ); ?></h4>
                        </a>
                    </div>
                    <!-- /.col -->
                <?php endwhile; ?>

            </div>
            <!-- /.row -->
        <?php endif; ?>
    </div>
    <!-- /.container -->
</main>

<?php
get_footer();
