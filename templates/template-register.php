<?php

/**
 * Template Name: Cadastro
 * Template Post Type: page
 *
 */

get_header();
?>

<main id="template-contact" class="bg-dark">

    <img class="img-overlap img-fluid" src="<?php echo get_template_directory_uri() . '/images/cracha.png'; ?>" alt="Crachá DataStore HUB EX 2020">    

    <div class="container">        
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="bg-color-two my-3 p-3">
                    <h1 class="font-weight-light">Cadastre-se para receber novidades do Hub e novos eventos da Datastore.</h1>
                    <?php echo do_shortcode( get_theme_mod( 'setting_form_shortcode' ) ); ?>
                </div>

                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();

                    get_template_part( 'template-parts/content/content', 'page' );

                endwhile; // End of the loop.
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</main>

<?php
get_footer('bigger');
