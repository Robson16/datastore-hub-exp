<?php

/**
 * Template Name: Apresentações
 * Template Post Type: page
 *
 */

get_header();
?>

<main>
    <div class="container py-5">
        <div class="row py-5">
            <div class="col-12">
                <?php
                while ( have_posts() ) {
                    the_post();
                    get_template_part( 'template-parts/content/content', 'page' );
                }
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <?php
        $speakers = new WP_Query( array(
            'post_type' => array( 'speakers' ),
            'post_status' => 'publish',
        ) );

        if ( $speakers->have_posts() ) :
        ?>
            <div class="row">
                <div class="col-12">
                    <h2 class="text-lowercase font-cocogoose text-color-three mb-5">Palestrantes</h2>
                </div>
                <?php while ( $speakers->have_posts() ) : $speakers->the_post() ?>
                    <div class="col-12 col-md-6">
                        <?php echo get_template_part( 'template-parts/content/content', 'speaker' ); ?>
                    </div>
                    <!-- /.col -->
                <?php endwhile; ?>
            </div>
            <!-- /.row -->
        <?php endif; ?>

        <?php
        $panelists = new WP_Query( array(
            'post_type' => array( 'panelists' ),
            'post_status' => 'publish',
        ) );

        if ( $panelists->have_posts() ) :
        ?>
            <div class="row">
                <div class="col-12">
                    <h2 class="text-lowercase font-cocogoose text-color-three mb-5">Painelista</h2>
                </div>
                <?php while ( $panelists->have_posts() ) : $panelists->the_post() ?>
                    <div class="col-12 col-md-6">
                        <?php echo get_template_part( 'template-parts/content/content', 'speaker' ); ?>
                    </div>
                    <!-- /.col -->
                <?php endwhile; ?>
            </div>
            <!-- /.row -->
        <?php endif; ?>
    </div>
    <!-- /.container -->
</main>

<?php
get_footer();
