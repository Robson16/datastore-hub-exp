<footer id="footer">
    <section class="footer-bigger bg-image-dark text-color-one">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <h5 class="footer-title font-cocogoose text-center text-md-left text-lowercase">
                        olha quem já é <br> hub<span class="text-white">xp</span>2020:
                    </h5>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <?php if ( get_theme_mod( 'setting_buylink_url' ) ):  ?>
                <div class="row ">
                    <div class="col-12 col-md-6">
                        <h5 class="footer-title text-center text-md-left text-lowercase">
                            <a href="<?php echo esc_html( get_theme_mod( 'setting_buylink_url', 'https://www.sympla.com.br/' ) ); ?>" class="font-weight-bold" target="_black" rel="noopener">
                                Clique aqui para comprar agora
                            </a>
                        </h5>
                    </div>
                </div>
                <!-- /.row -->
            <?php endif; ?>

            <div class="row ">
                <?php if (is_active_sidebar('dtshubxp20-sidebar-footer-1')) : ?>
                    <div class="col-6 col-md-6 col-lg-3 py-3">
                        <?php dynamic_sidebar('dtshubxp20-sidebar-footer-1'); ?>
                    </div>
                    <!-- /.col -->
                <?php endif; ?>

                <?php if (is_active_sidebar('dtshubxp20-sidebar-footer-2')) : ?>
                    <div class="col-6 col-md-6 col-lg-3 py-3">
                        <?php dynamic_sidebar('dtshubxp20-sidebar-footer-2'); ?>
                    </div>
                    <!-- /.col -->
                <?php endif; ?>

                <?php if (is_active_sidebar('dtshubxp20-sidebar-footer-3')) : ?>
                    <div class="col-6 col-md-6 col-lg-3 py-3">
                        <?php dynamic_sidebar('dtshubxp20-sidebar-footer-3'); ?>
                    </div>
                    <!-- /.col -->
                <?php endif; ?>

                <?php if (is_active_sidebar('dtshubxp20-sidebar-footer-4')) : ?>
                    <div class="col-6 col-md-6 col-lg-3 py-3">
                        <?php dynamic_sidebar('dtshubxp20-sidebar-footer-4'); ?>
                    </div>
                    <!-- /.col -->
                <?php endif; ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <?php get_template_part( 'template-parts/footer/footer', 'navbar' ); ?>
    <?php get_template_part( 'template-parts/footer/footer', 'copyright' ); ?>
</footer>

<?php wp_footer(); ?>

</body>

</html>